#!/bin/bash

# IFS=' '
TEMP_PATH=$(mktemp -d)
OUTPUTFILE=${TEMP_PATH}/outfile.conf
TARGETFILE="/etc/nginx/sites-available/10_upstream.conf"

hosts=($(dig ${UPSTREAM_HOSTNAME} +short | sort));
if [ ${#hosts[@]} -eq 0 ]; then
	# No hosts found - should this be another address?
	hosts=("127.0.0.1")
fi


echo "upstream caches {" | tee -a $OUTPUTFILE
echo '    hash $cacheidentifier$uri$slice_range;' | tee -a $OUTPUTFILE

for i in "${hosts[@]}"; do
	echo "    server ${i}:${UPSTREAM_PORT} fail_timeout=1m max_fails=1;" | tee -a $OUTPUTFILE
done

echo "}" | tee -a $OUTPUTFILE;

cp $OUTPUTFILE $TARGETFILE
rm -rf $TEMP_PATH