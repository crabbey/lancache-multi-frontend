FROM steamcache/monolithic:latest

ENV UPSTREAM_HOSTNAME="lancache.please.set.this.tld"
ENV UPSTREAM_PORT="8080"


COPY overlay/ /

RUN rm /etc/nginx/sites-enabled/*; \
	apt-get update; \
	apt-get install -y dnsutils; \
	ln -s /etc/nginx/sites-available/10_upstream.conf /etc/nginx/sites-enabled/10_upstream.conf; \
	ln -s /etc/nginx/sites-available/20_frontend.conf /etc/nginx/sites-enabled/20_frontend.conf


VOLUME ["/data/logs", "/data/cachedomains"]

EXPOSE 80

WORKDIR /scripts